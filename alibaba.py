import pygame
import random


pygame.init()


display_width = 800
display_height = 600


#color
white = (255, 255, 255)
black = (0, 0, 0)
red = (200, 0, 0)
light_red = (255, 0 ,0)
blue = (0, 128, 255)
yellow = (200, 200, 0)
light_yellow = (255, 255, 0)
green = (34, 177, 76)
light_green = (0, 255, 0)


gameDisplay = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Alibaba's Adventure")

bg = pygame.image.load("pics/bg.jpg")
icon = pygame.image.load("pics/SM_ATKleft_1.png")
pygame.display.set_icon(icon)


#Swordman
SM_right = [pygame.image.load("pics/SM_right_1.png"),
         pygame.image.load("pics/SM_right_2.png"),
         pygame.image.load("pics/SM_right_1.png"),
         pygame.image.load("pics/SM_right_3.png")]
SM_ATKright = [pygame.image.load("pics/SM_ATKright_1.png"),
               pygame.image.load("pics/SM_ATKright_2.png"),
               pygame.image.load("pics/SM_ATKright_3.png")]
SM_left = [pygame.image.load("pics/SM_left_1.png"),
           pygame.image.load("pics/SM_left_2.png"),
           pygame.image.load("pics/SM_left_1.png"),
           pygame.image.load("pics/SM_left_3.png")]
SM_ATKleft = [pygame.image.load("pics/SM_ATKleft_1.png"),
              pygame.image.load("pics/SM_ATKleft_2.png"),
              pygame.image.load("pics/SM_ATKleft_3.png")]
SM_up = [pygame.image.load("pics/SM_up_1.png"),
         pygame.image.load("pics/SM_up_2.png"),
         pygame.image.load("pics/SM_up_1.png"),
         pygame.image.load("pics/SM_up_3.png")]
SM_ATKup = [pygame.image.load("pics/SM_ATKup_1.png"),
            pygame.image.load("pics/SM_ATKup_2.png"),
            pygame.image.load("pics/SM_ATKup_3.png")]
SM_down = [pygame.image.load("pics/SM_down_1.png"),
           pygame.image.load("pics/SM_down_2.png"),
           pygame.image.load("pics/SM_down_1.png"),
           pygame.image.load("pics/SM_down_3.png")]
SM_ATKdown = [pygame.image.load("pics/SM_ATKdown_1.png"),
              pygame.image.load("pics/SM_ATKdown_2.png"),
              pygame.image.load("pics/SM_ATKdown_3.png")]
#Archer
AH_right = [pygame.image.load("pics/AH_right_1.png"),
            pygame.image.load("pics/AH_right_2.png"),
            pygame.image.load("pics/AH_right_1.png"),
            pygame.image.load("pics/AH_right_3.png")]
AH_ATKright = [pygame.image.load("pics/AH_ATKright_1.png"),
               pygame.image.load("pics/AH_ATKright_2.png"),
               pygame.image.load("pics/AH_ATKright_3.png")]
AH_left = [pygame.image.load("pics/AH_left_1.png"),
           pygame.image.load("pics/AH_left_2.png"),
           pygame.image.load("pics/AH_left_1.png"),
           pygame.image.load("pics/AH_left_3.png")]
AH_ATKleft = [pygame.image.load("pics/AH_ATKleft_1.png"),
              pygame.image.load("pics/AH_ATKleft_2.png"),
              pygame.image.load("pics/AH_ATKleft_3.png")]
AH_up = [pygame.image.load("pics/AH_up_1.png"),
         pygame.image.load("pics/AH_up_2.png"),
         pygame.image.load("pics/AH_up_1.png"),
         pygame.image.load("pics/AH_up_3.png")]
AH_ATKup = [pygame.image.load("pics/AH_ATKup_1.png"),
            pygame.image.load("pics/AH_ATKup_2.png"),
            pygame.image.load("pics/AH_ATKup_3.png")]
AH_down = [pygame.image.load("pics/AH_down_1.png"),
           pygame.image.load("pics/AH_down_2.png"),
           pygame.image.load("pics/AH_down_1.png"),
           pygame.image.load("pics/AH_down_3.png")]
AH_ATKdown = [pygame.image.load("pics/AH_ATKdown_1.png"),
              pygame.image.load("pics/AH_ATKdown_2.png"),
              pygame.image.load("pics/AH_ATKdown_3.png")]
#Wizard
WZ_right = [pygame.image.load("pics/WZ_right_1.png"),
            pygame.image.load("pics/WZ_right_2.png"),
            pygame.image.load("pics/WZ_right_1.png"),
            pygame.image.load("pics/WZ_right_3.png")]
WZ_ATKright = [pygame.image.load("pics/WZ_ATKright_1.png"),
               pygame.image.load("pics/WZ_ATKright_2.png"),
               pygame.image.load("pics/WZ_ATKright_3.png")]
WZ_left = [pygame.image.load("pics/WZ_left_1.png"),
           pygame.image.load("pics/WZ_left_2.png"),
           pygame.image.load("pics/WZ_left_1.png"),
           pygame.image.load("pics/WZ_left_3.png")]
WZ_ATKleft = [pygame.image.load("pics/WZ_ATKleft_1.png"),
              pygame.image.load("pics/WZ_ATKleft_2.png"),
              pygame.image.load("pics/WZ_ATKleft_3.png")]
WZ_up = [pygame.image.load("pics/WZ_up_1.png"),
         pygame.image.load("pics/WZ_up_2.png"),
         pygame.image.load("pics/WZ_up_1.png"),
         pygame.image.load("pics/WZ_up_3.png")]
WZ_ATKup = [pygame.image.load("pics/WZ_ATKup_1.png"),
            pygame.image.load("pics/WZ_ATKup_2.png"),
            pygame.image.load("pics/WZ_ATKup_3.png")]
WZ_down = [pygame.image.load("pics/WZ_down_1.png"),
           pygame.image.load("pics/WZ_down_2.png"),
           pygame.image.load("pics/WZ_down_1.png"),
           pygame.image.load("pics/WZ_down_3.png")]
WZ_ATKdown = [pygame.image.load("pics/WZ_ATKdown_1.png"),
              pygame.image.load("pics/WZ_ATKdown_2.png"),
              pygame.image.load("pics/WZ_ATKdown_3.png")]
#Orc
O_right = [pygame.image.load("pics/O_right_1.png"),
            pygame.image.load("pics/O_right_2.png"),
            pygame.image.load("pics/O_right_1.png"),
            pygame.image.load("pics/O_right_3.png")]
##O_ATKright = [pygame.image.load("pics/O_ATKright_1.png"),
##               pygame.image.load("pics/O_ATKright_2.png"),
##               pygame.image.load("pics/O_ATKright_3.png")]
O_left = [pygame.image.load("pics/O_left_1.png"),
           pygame.image.load("pics/O_left_2.png"),
           pygame.image.load("pics/O_left_1.png"),
           pygame.image.load("pics/O_left_3.png")]
##O_ATKleft = [pygame.image.load("pics/O_ATKleft_1.png"),
##              pygame.image.load("pics/O_ATKleft_2.png"),
##              pygame.image.load("pics/O_ATKleft_3.png")]
O_up = [pygame.image.load("pics/O_up_1.png"),
         pygame.image.load("pics/O_up_2.png"),
         pygame.image.load("pics/O_up_1.png"),
         pygame.image.load("pics/O_up_3.png")]
##O_ATKup = [pygame.image.load("pics/O_ATKup_1.png"),
##            pygame.image.load("pics/O_ATKup_2.png"),
##            pygame.image.load("pics/O_ATKup_3.png")]
O_down = [pygame.image.load("pics/O_down_1.png"),
           pygame.image.load("pics/O_down_2.png"),
           pygame.image.load("pics/O_down_1.png"),
           pygame.image.load("pics/O_down_3.png")]
##O_ATKdown = [pygame.image.load("pics/O_ATKdown_1.png"),
##              pygame.image.load("pics/O_ATKdown_2.png"),
##              pygame.image.load("pics/O_ATKdown_3.png")]
#Skeleton
SK_right = [pygame.image.load("pics/SK_right_1.png"),
            pygame.image.load("pics/SK_right_2.png"),
            pygame.image.load("pics/SK_right_1.png"),
            pygame.image.load("pics/SK_right_3.png")]
##SK_ATKright = [pygame.image.load("pics/SK_ATKright_1.png"),
##               pygame.image.load("pics/SK_ATKright_2.png"),
##               pygame.image.load("pics/SK_ATKright_3.png")]
SK_left = [pygame.image.load("pics/SK_left_1.png"),
           pygame.image.load("pics/SK_left_2.png"),
           pygame.image.load("pics/SK_left_1.png"),
           pygame.image.load("pics/SK_left_3.png")]
##SK_ATKleft = [pygame.image.load("pics/SK_ATKleft_1.png"),
##              pygame.image.load("pics/SK_ATKleft_2.png"),
##              pygame.image.load("pics/SK_ATKleft_3.png")]
SK_up = [pygame.image.load("pics/SK_up_1.png"),
         pygame.image.load("pics/SK_up_2.png"),
         pygame.image.load("pics/SK_up_1.png"),
         pygame.image.load("pics/SK_up_3.png")]
##SK_ATKup = [pygame.image.load("pics/SK_ATKup_1.png"),
##            pygame.image.load("pics/SK_ATKup_2.png"),
##            pygame.image.load("pics/SK_ATKup_3.png")]
SK_down = [pygame.image.load("pics/SK_down_1.png"),
           pygame.image.load("pics/SK_down_2.png"),
           pygame.image.load("pics/SK_down_1.png"),
           pygame.image.load("pics/SK_down_3.png")]
##SK_ATKdown = [pygame.image.load("pics/SK_ATKdown_1.png"),
##              pygame.image.load("pics/SK_ATKdown_2.png"),
##              pygame.image.load("pics/SK_ATKdown_3.png")]
#DarkElf
DE_right = [pygame.image.load("pics/DE_right_1.png"),
            pygame.image.load("pics/DE_right_2.png"),
            pygame.image.load("pics/DE_right_1.png"),
            pygame.image.load("pics/DE_right_3.png")]
##DE_ATKright = [pygame.image.load("pics/DE_ATKright_1.png"),
##               pygame.image.load("pics/DE_ATKright_2.png"),
##               pygame.image.load("pics/DE_ATKright_3.png")]
DE_left = [pygame.image.load("pics/DE_left_1.png"),
           pygame.image.load("pics/DE_left_2.png"),
           pygame.image.load("pics/DE_left_1.png"),
           pygame.image.load("pics/DE_left_3.png")]
##DE_ATKleft = [pygame.image.load("pics/DE_ATKleft_1.png"),
##              pygame.image.load("pics/DE_ATKleft_2.png"),
##              pygame.image.load("pics/DE_ATKleft_3.png")]
DE_up = [pygame.image.load("pics/DE_up_1.png"),
         pygame.image.load("pics/DE_up_2.png"),
         pygame.image.load("pics/DE_up_1.png"),
         pygame.image.load("pics/DE_up_3.png")]
##DE_ATKup = [pygame.image.load("pics/DE_ATKup_1.png"),
##            pygame.image.load("pics/DE_ATKup_2.png"),
##            pygame.image.load("pics/DE_ATKup_3.png")]
DE_down = [pygame.image.load("pics/DE_down_1.png"),
           pygame.image.load("pics/DE_down_2.png"),
           pygame.image.load("pics/DE_down_1.png"),
           pygame.image.load("pics/DE_down_3.png")]
##DE_ATKdown = [pygame.image.load("pics/DE_ATKdown_1.png"),
##              pygame.image.load("pics/DE_ATKdown_2.png"),
##              pygame.image.load("pics/DE_ATKdown_3.png")]



clock = pygame.time.Clock()

blockWidth = 30
blockHeight = 50

FPS = 30

#font
smallfont = pygame.font.SysFont("comicsansms", 25)
mediumfont = pygame.font.SysFont("comicsansms", 50)
largefont = pygame.font.SysFont("comicsansms", 70)


def pause():
    paused = True

    message_to_screen("Paused",
                      yellow,
                      -50,
                      size = "large")
    message_to_screen("Press C to continue or Q to quit.",
                      yellow,
                      50)
    pygame.display.update()
    clock.tick(15)

    while paused:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_c:
                    paused = False

                elif event.key == pygame.K_q:
                    pygame.quit()
                    quit()
                    
        #gameDisplay.fill(white)

        clock.tick(15)


def score(total_score):
    text = smallfont.render("Score: " + "   " + str(total_score), True, yellow)
    gameDisplay.blit(text,[650,0])


def randMonGen():

    randMonX = round(random.randrange(blockWidth*2,display_width - blockWidth*2)/10.0)*10
    randMonY = round(random.randrange(blockHeight*2,display_height - blockHeight*2)/10.0)*10

    MonType = random.choice(["Orc","Orc","Orc","Orc","Orc","SK","SK","SK","DE"])

    return randMonX, randMonY, MonType


def game_intro():

    intro = True

    walk_times = 0
    ani_times = 0
    SM = SM_right
    AH = AH_right
    WZ = WZ_right
    O = O_left
    SK = SK_left
    DE = DE_left
    SMpos = [[300,325]]
    AHpos = [[250,325]]
    WZpos = [[200,325]]
    Opos = [[480,331]]
    SKpos = [[520,325]]
    DEpos = [[590,325]]

    while intro:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
                    
        gameDisplay.fill(white)
        message_to_screen("Alibaba's Adventure",
                          green,
                          -100,
                          "large")

        
        gameDisplay.blit(SM[walk_times], ([300,325]))
        gameDisplay.blit(AH[walk_times], ([250,325]))
        gameDisplay.blit(WZ[walk_times], ([200,325]))
        gameDisplay.blit(O[walk_times], ([480,331]))
        gameDisplay.blit(SK[walk_times], ([520,325]))
        gameDisplay.blit(DE[walk_times], ([590,325]))
        
        ani_times += 1
        if ani_times==4:
            walk_times += 1
            if walk_times == 4:
                walk_times = 0
            ani_times = 0

        
        
        button("PLAY", 150, 500, 100, 50, green, light_green, action="play")
        button("HELP", 350, 500, 100, 50, yellow, light_yellow, action="help")
        button("QUIT", 550, 500, 100, 50, red, light_red, action="quit")

        
        pygame.display.update()
        clock.tick(15)

                                
def party_walk(partyList, direcList, heroList, walk_times, walk):


    gameDisplay.blit(walk[walk_times], (partyList[-1][0], partyList[-1][1]))


    for hero in heroList:
        if hero == "Swordman":
            pass

        elif hero == "Archer":

            if direcList[heroList.index(hero)] == "right":
                walk = AH_right
                posX = -blockWidth*heroList.index(hero)
                posY = 0
        
            if direcList[heroList.index(hero)] == "left":
                walk = AH_left
                posX = blockWidth*heroList.index(hero)
                posY = 0
        
            if direcList[heroList.index(hero)] == "up":
                walk = AH_up
                posY = blockWidth*heroList.index(hero)
                posX = 0
       
            if direcList[heroList.index(hero)] == "down":
                walk = AH_down
                posY = -blockWidth*heroList.index(hero)
                posX = 0


            gameDisplay.blit(walk[walk_times], (partyList[-heroList.index(hero) - 1][0] + posX, partyList[-heroList.index(hero) - 1][1] + posY))
                
        elif hero == "Wizard":
            
            if direcList[heroList.index(hero)] == "right":
                walk = WZ_right
                posX = -blockWidth*heroList.index(hero)
                posY = 0
        
            if direcList[heroList.index(hero)] == "left":
                walk = WZ_left
                posX = blockWidth*heroList.index(hero)
                posY = 0
        
            if direcList[heroList.index(hero)] == "up":
                walk = WZ_up
                posY = +blockWidth*heroList.index(hero)
                posX = 0
       
            if direcList[heroList.index(hero)] == "down":
                walk = WZ_down
                posY = -blockWidth*heroList.index(hero)
                posX = 0


            gameDisplay.blit(walk[walk_times], (partyList[-heroList.index(hero) - 1][0] + posX, partyList[-heroList.index(hero) - 1][1] + posY))
        


def party_atk(partyList, direcList, heroList, atk_times, atk):


    if direction == "right":
        SWposX = 0
        SWposY = 0
        
    if direction == "left":
        SWposX = -blockWidth*2
        SWposY = 0

    if direction == "up":
        SWposX = -blockWidth/2
        SWposY = 0

    if direction == "down":
        SWposX = -blockWidth/2
        SWposY = 0
        
    gameDisplay.blit(atk[atk_times], (partyList[-1][0] + SWposX, partyList[-1][1] + SWposY))

    for hero in heroList:
        if hero == "Swordman":
            pass

        elif hero == "Archer":

            if direcList[heroList.index(hero)] == "right":
                atk = AH_ATKright
                posX = -blockWidth*heroList.index(hero)
                posY = 0
        
            if direcList[heroList.index(hero)] == "left":
                atk = AH_ATKleft
                posX = blockWidth*heroList.index(hero)
                posY = 0
        
            if direcList[heroList.index(hero)] == "up":
                atk = AH_ATKup
                posY = blockWidth*heroList.index(hero)
                posX = 0
       
            if direcList[heroList.index(hero)] == "down":
                atk = AH_ATKdown
                posY = -blockWidth*heroList.index(hero)
                posX = 0

            gameDisplay.blit(atk[atk_times], (partyList[-heroList.index(hero) - 1][0] + posX, partyList[-heroList.index(hero) - 1][1] + posY))
                
        elif hero == "Wizard":
            
            if direcList[heroList.index(hero)] == "right":
                atk = WZ_ATKright
                posX = -blockWidth*heroList.index(hero)
                posY = 0
        
            if direcList[heroList.index(hero)] == "left":
                atk = WZ_ATKleft
                posX = blockWidth*heroList.index(hero)
                posY = 0
        
            if direcList[heroList.index(hero)] == "up":
                atk = WZ_ATKup
                posY = +blockWidth*heroList.index(hero)
                posX = 0
       
            if direcList[heroList.index(hero)] == "down":
                atk = WZ_ATKdown
                posY = -blockWidth*heroList.index(hero)
                posX = 0

            gameDisplay.blit(atk[atk_times], (partyList[-heroList.index(hero) - 1][0] + posX, partyList[-heroList.index(hero) - 1][1] + posY))



def Mon_walk(randMonX, randMonY, MonDirec, mon_walk_times, MonX_change, MonY_change, monwalk):

    gameDisplay.blit(monwalk[mon_walk_times], (randMonX, randMonY))

    
def health_bars(player_health, monster_health, randMonX, randMonY, MonType):

    if player_health > 60:
        player_health_color = green
    elif player_health > 30:
        player_health_color = yellow
    else:
        player_health_color = red

    if monster_health > 60:
        monster_health_color = green
    elif player_health > 30:
        monster_health_color = yellow
    else:
        monster_health_color = red

    pygame.draw.rect(gameDisplay, player_health_color, (25, 25, player_health*2, 20))

    if MonType == "Orc":
        monster_health /= 4
    elif MonType == "SK":
        monster_health /= 6
    elif MonType == "DE":
        monster_health /= 8
    pygame.draw.rect(gameDisplay, monster_health_color, (randMonX, randMonY - 10, monster_health, 5))

def text_objects(text, color, size):
    if size == "small":
        textSurface = smallfont.render(text, True, color)
    elif size == "medium":
        textSurface = mediumfont.render(text, True, color)
    elif size == "large":
        textSurface = largefont.render(text, True, color)
    return textSurface, textSurface.get_rect()

        
def message_to_screen(msg, color, y_displace = 0, size = "small"):

    textSurf, textRect = text_objects(msg, color, size)
    textRect.center = (display_width / 2), (display_height / 2 + y_displace)
    gameDisplay.blit(textSurf, textRect)


def text_to_button(msg, color, buttonx, buttony, buttonwidth, buttonheight, size = "small"):
    textSurf, textRect = text_objects(msg, color, size)
    textRect.center = ((buttonx + (buttonwidth/2)), buttony + (buttonheight/2))
    gameDisplay.blit(textSurf, textRect)


def game_help():

    ghelp = True

    while ghelp:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
                    
        gameDisplay.fill(white)
        message_to_screen("HELP",
                          yellow,
                          -200,
                          "large")
        message_to_screen("The objective of the game is to defeat all monster",
                          black,
                          -75)
        message_to_screen("The more monster that you kill, the more party you get",
                          black,
                          -35)
        message_to_screen("If you run into yourself, or the edges, YOU DIE!!",
                          red,
                          5)
        message_to_screen("Beware!! the Dark Elf",
                          blue,
                          35)
        message_to_screen("Move: WASD",
                          black,
                          90)
        message_to_screen("Pause: P",
                          black,
                          115)
        message_to_screen("ATK: auto",
                          black,
                          140)


        button("PLAY", 150, 500, 100, 50, green, light_green, action="play")
        button("QUIT", 550, 500, 100, 50, red, light_red, action="quit")

        
        pygame.display.update()
        clock.tick(15)


def button(text, x, y, width, height, inactive_color, active_color, action):
    cur = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if x + width > cur[0] > x and y + height > cur[1] > y:
        pygame.draw.rect(gameDisplay, active_color, (x, y, width, height))
        if click[0] == 1 and action != None:
            
            if action == "quit":
                pygame.quit()
                quit()

            if action == "help":
                game_help()

            if action == "play":
                gameLoop()

            if action == "try":
                gameLoop()

                
    else:
        pygame.draw.rect(gameDisplay, inactive_color, (x, y, width, height))

    text_to_button(text, black, x, y, width, height)

    
def gameLoop():
    global direction
    direction = "right"
    MonDirec = "right"
    gameExit = False
    gameOver = False

    lead_x = display_width/2
    lead_y = display_height/2
    lead_x_change = blockWidth/6
    lead_y_change = 0
    HeroSpeed = blockWidth/6
    OrcSpeed = 2
    SKSpeed = 4
    DESpeed = 8
    MonSpeed = 2
    MonX_change = 2
    MonY_change = 0

    heroList = ["Swordman"]
    partyList = []
    direcList = []
    atk_times = 0
    atk_ani = 0
    ani_times = 0
    walk_times = 0
    mon_times = 0
    mon_ani = 0
    mon_walk_times = 0

    player_health = 100
    Orc_health = 100
    SK_health = 150
    DE_health = 200
    monster_health = 100

    more_hero = "Nope"

    randMonX, randMonY, MonType = randMonGen()
    MonType = "Orc"

    total_score = 0
    
    randMonX = round(random.randrange(0,display_width - blockWidth)/10.0)*10
    randMonY = round(random.randrange(0,display_height - blockHeight)/10.0)*10
    
    while not gameExit:
        
        while gameOver == True:

            gameDisplay.fill(white)

            message_to_screen("Game Over",
                              red,
                              y_displace = -75,
                              size = "large")
            message_to_screen("Score: " + str(total_score),
                              yellow,
                              y_displace = -10,
                              size = "small")

            button("TRY AGAIN", 200, 350, 150, 50, green, light_green, action="try")
            button("QUIT", 475, 350, 150, 50, red, light_red, action="quit")
            
            pygame.display.update()

            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    gameExit = True
                    gameOver = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        gameExit = True
                        gameOver = False
                    if event.key == pygame.K_c:
                        gameLoop()
            
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameExit = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    if direction == "right":
                        pass
                    else:
                        direction = "left"
                        lead_x_change = -HeroSpeed
                        lead_y_change = 0
                elif event.key == pygame.K_d:
                    if direction == "left":
                        pass
                    else:
                        direction = "right"
                        lead_x_change = HeroSpeed 
                        lead_y_change = 0
                elif event.key == pygame.K_w:
                    if direction == "down":
                        pass
                    else:
                        direction = "up"
                        lead_y_change = -HeroSpeed
                        lead_x_change = 0
                elif event.key == pygame.K_s:
                    if direction == "up":
                        pass
                    else:
                        direction = "down"
                        lead_y_change = HeroSpeed
                        lead_x_change = 0

                elif event.key == pygame.K_p:
                    pause()
                

              
        if lead_x >= display_width or lead_x < 0 or lead_y >= display_height or lead_y < 0:
            gameOver = True
            
        lead_x += lead_x_change
        lead_y += lead_y_change
        
        gameDisplay.blit(bg, (0, 0))

        if direction == "right":
            walk = SM_right
            atk = SM_ATKright
        
        if direction == "left":
            walk = SM_left
            atk = SM_ATKleft

        if direction == "up":
            walk = SM_up
            atk = SM_ATKup

        if direction == "down":
            walk = SM_down
            atk = SM_ATKdown


        direcList.append(direction)

        partyHead = []
        partyHead.append(lead_x)
        partyHead.append(lead_y)
        partyList.append(partyHead)
        if len(partyList) > len(heroList):
            del partyList[0]
        if len(direcList) > len(heroList):
            del direcList[0]
        for eachSegment in partyList[:-1]:
            if eachSegment == partyHead:
                gameOver = True



        if lead_x < randMonX + blockWidth + blockWidth*1.5 and lead_x > randMonX - blockWidth*1.5  or lead_x + blockWidth > randMonX - blockWidth*1.5 and lead_x + blockWidth < randMonX + blockWidth + - blockWidth*1.5:
        
            if lead_y < randMonY + blockHeight + blockHeight and lead_y > randMonY - blockHeight:
                party_atk(partyList, direcList, heroList, atk_times, atk)
                
                atk_ani += 1
                if ani_times==3:
                    atk_times += 1
                    if atk_times == 3:
                        atk_times = 0
                    if "Archer" in heroList and "Wizard" in heroList:
                        monster_health -= 120
                    elif "Archer" in heroList or "Wizard" in heroList:
                        monster_health -= 90
                    else:
                        monster_health -= 60
                    atk_ani = 0

   
            elif lead_y + blockHeight < randMonY + blockHeight + blockHeight and lead_y + blockHeight > randMonY - blockHeight:
                party_atk(partyList, direcList, heroList, atk_times, atk)
                
                atk_ani += 1
                if ani_times==3:
                    atk_times += 1
                    if atk_times == 3:
                        atk_times = 0
                    if "Archer" in heroList and "Wizard" in heroList:
                        monster_health -= 120
                    elif "Archer" in heroList or "Wizard" in heroList:
                        monster_health -= 90
                    else:
                        monster_health -= 60
                    atk_ani = 0
                 

            else:
                party_walk(partyList, direcList, heroList, walk_times, walk)

        else:
            party_walk(partyList, direcList, heroList, walk_times, walk)


        if monster_health <= 0:
            if MonType == "Orc":
                total_score += 1
            elif MonType == "SK":
                total_score += 2
            elif MonType == "DE":
                total_score += 5
            if more_hero == "Nope" and len(heroList) < 3:
                if "Archer" not in heroList and "Wizard" not in heroList:
                    more_hero = random.choice(["Archer", "Wizard","Nope", "Nope", "Nope", "Nope", "Nope", "Nope"])
                elif "Archer" in heroList:
                    more_hero = random.choice(["Wizard"])
                elif "Wizard" in heroList:
                    more_hero = random.choice(["Archer"])
                if more_hero == "Archer":
                    heroX, heroY, MonType = randMonGen()
                    time_remain = 200
                elif more_hero == "Wizard":
                    heroX, heroY, MonType = randMonGen()
                    time_remain = 200
            randMonX, randMonY, MonType = randMonGen()
            if MonType == "Orc":
                MonSpeed = OrcSpeed
                monster_health = Orc_health
            elif MonType == "SK":
                MonSpeed = SKSpeed
                monster_health = SK_health
            elif MonType == "DE":
                MonSpeed = DESpeed
                monster_health = DE_health


        if more_hero != "Nope":
            if time_remain <= 0:
                more_hero = "Nope"
            if more_hero == "Archer":
                gameDisplay.blit(AH_down[0], (heroX, heroY))
                time_remain -= 1
            elif more_hero == "Wizard":
                gameDisplay.blit(WZ_down[0], (heroX, heroY))
                time_remain -= 1
            if lead_x*10 > heroX and lead_x < heroX + blockWidth or lead_x + blockWidth > heroX and lead_x + blockWidth < heroX + blockWidth:
            
                if lead_y > heroY and lead_y < heroY + blockHeight:
                    heroList.append(more_hero)
                    more_hero = "Nope"
                
                elif lead_y + blockHeight > heroY and lead_y + blockHeight < heroY + blockHeight:
                    heroList.append(more_hero)
                    more_hero = "Nope"


        ani_times += 1
        if ani_times==8:
            walk_times += 1
            if walk_times == 4:
                walk_times = 0
            ani_times = 0


        if MonDirec == "right":
            MonX_change = MonSpeed
            MonY_change = 0
            if MonType == "Orc":
                monwalk = O_right
            elif MonType == "SK":
                monwalk = SK_right
            elif MonType == "DE":
                monwalk = DE_right

        elif MonDirec == "left":
            MonX_change = -MonSpeed
            MonY_change = 0
            if MonType == "Orc":
                monwalk = O_left
            elif MonType == "SK":
                monwalk = SK_left
            elif MonType == "DE":
                monwalk = DE_left

        elif MonDirec == "up":
            MonY_change = -MonSpeed
            MonX_change = 0
            if MonType == "Orc":
                monwalk = O_up
            elif MonType == "SK":
                monwalk = SK_up
            elif MonType == "DE":
                monwalk = DE_up
    
        elif MonDirec == "down":
            MonY_change = MonSpeed
            MonX_change = 0
            if MonType == "Orc":
                monwalk = O_down
            elif MonType == "SK":
                monwalk = SK_down
            elif MonType == "DE":
                monwalk = DE_down

        randMonX += MonX_change
        randMonY += MonY_change
        
        Mon_walk(randMonX, randMonY, MonDirec, mon_walk_times, MonX_change, MonY_change, monwalk)

        mon_ani += 1
        if mon_ani==4:
            mon_walk_times += 1
            if mon_walk_times == 4:
                mon_walk_times = 0
            mon_ani = 0

        mon_times += 1

        if mon_times >= 100:
            MonDirec = random.choice(["up", "right", "down", "left"])
            mon_times = 0
        if randMonX > 750 and MonDirec == "right":
            MonDirec = random.choice(["up", "down"])
        if randMonX < 50 and MonDirec == "left" :
            MonDirec = random.choice(["up", "down"])   
        if randMonY > 550 and MonDirec == "down":
            MonDirec = random.choice(["right", "left"])
        if randMonY < 50 and MonDirec == "up":
            MonDirec = random.choice(["right", "left"])

        

        score(total_score)


        health_bars(player_health, monster_health, randMonX, randMonY, MonType)

        pygame.display.update()


        if lead_x > randMonX and lead_x < randMonX + blockWidth or lead_x + blockWidth > randMonX and lead_x + blockWidth < randMonX + blockWidth:
            
            if lead_y > randMonY and lead_y < randMonY + blockHeight:
                pass

                gameOver = True
                
            elif lead_y + blockHeight > randMonY and lead_y + blockHeight < randMonY + blockHeight:
                pass
                
                gameOver = True


            
        clock.tick(FPS)

    pygame.quit()
    quit()

game_intro()    
gameLoop()
